#### Info 
To work out the content size, I have used the content-length header. I know this is not a mandatory field but it seems to be 
correct in this case.

It was not clear where to get the description field. I have fetched the description by following the link to the product info page.

#### To run tests
    
    mvn test
    
#### To package into an executable jar. The jar will be placed at target/Scraper.jar

    mvn package
    
#### Once packaged, to run the jar use:

    java -jar Scraper.jar <url to product page>
    
##### for example:

    java -jar Scraper.jar http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html


#### Libraries used (defined in the pom)
  * gson 
  * jsoup
  * junit
    