package com.rizvn.sainsburys;

import com.google.gson.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Class to scrape sainsburys products pages
 * Created by Riz
 */
public class Scraper {

  private final static MathContext mc = new MathContext(10);
  private final Gson gson             = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();

  /**
   * Fetch page for url
   * @param url url of page
   * @return Jsoup Document
   */
  protected Document fetchPage(String url){
    try{
      return Jsoup.parse(new URL(url), 30000);
    }catch (IOException ex){
      //convert to runtime exception
      throw new IllegalArgumentException(String.format("Could not fetch url: %s ", url));
    }
  }

  /**
   * Extract product data from page
   * @param page page for which to extract product data
   * @return json reperesntation of extratced data
   */
  protected String extractData(Document page){
    JsonObject json   = new JsonObject();
    JsonArray results = new JsonArray();
    BigDecimal total  = new BigDecimal(0.0, mc).setScale(2, BigDecimal.ROUND_CEILING);

    //select all products on page
    Elements products = page.select(".product");

    //loop though selected products
    for(Element product : products){
      JsonObject productJson = new JsonObject();
      String title          = product.select(".productInfo h3 a").text().trim();
      String pageLink       = product.select(".productInfo h3 a").attr("href");
      String pageSize       = getPageSize(pageLink);
      String description    = extractDescription(pageLink);

                                //regex to replace non numeric chars
      String unitPriceStr       = product.select("p.pricePerUnit").text().trim().replaceAll("[^\\d|^\\\\.]","");
      BigDecimal unitPrice      = new BigDecimal(unitPriceStr, mc).setScale(2, BigDecimal.ROUND_CEILING);
      total = total.add(unitPrice);

      productJson.add("title",       new JsonPrimitive(title));
      productJson.add("size",        new JsonPrimitive(pageSize));
      productJson.add("unit_price",  new JsonPrimitive(unitPrice));
      productJson.add("description", new JsonPrimitive(description));

      results.add(productJson);
    }

    json.add("results", results);
    json.add("total", new JsonPrimitive(total));

    return gson.toJson(json);
  }

  /**
   * Determine the size of page using the content header
   * @param url url to retrieve page size
   * @return size with kb suffix
   */
  protected String getPageSize(String url){
    try {
      HttpURLConnection page = (HttpURLConnection) new URL(url).openConnection();
      double size =  page.getContentLength()  /  1024.0;
      return new BigDecimal(size, mc).setScale(1, BigDecimal.ROUND_CEILING)+ "kb";
    }
    catch (IOException aEx)
    {
      throw new IllegalArgumentException(String.format("Could not fetch page %s", url));
    }
  }

  /**
   * Extract description, from products page
   * @param url url of the product page
   * @return description section content
   */
  protected String extractDescription(String url){
    Document page = fetchPage(url);
    return page.select(".productText p").first().text().trim();
  }

  /**
   * Takes a url products page return data in json form
   * @param url url of the products page
   * @return json response
   */
  public String scrape(String url){
    Document document = fetchPage(url);
    return extractData(document);
  }

  /**
   * Main method to run as app
   * @param args
   */
  public static void main(String [] args){
    Scraper scraper = new Scraper();
    System.out.println(scraper.scrape(args[0]));
  }
}
