package com.rizvn.sainsburys;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Riz
 */
public class ScraperTest {

  //subject under test
  Scraper sut;

  @Before
  public void setUp(){
    sut = new Scraper();
  }

  @Test
  public void testFetchPage(){
    Document page =  sut.fetchPage("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html");
    Assert.assertNotNull("Page could not be retrieved", page);
  }

  @Test
  public void testExtractData() throws Exception{
    Document page =  sut.fetchPage("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html");
    String jsonResponse = sut.extractData(page);

    //test some result output
    JsonParser jsonParser = new JsonParser();

    JsonObject json = jsonParser.parse(jsonResponse).getAsJsonObject();
    JsonArray results = json.getAsJsonArray("results");
    Assert.assertTrue(results.size() == 7);
  }

  @Test
  public void testGetPageSize(){
    String pageSize = sut.getPageSize("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-avocado--ripe---ready-x2.html");
    Assert.assertEquals("43.5kb", pageSize);
  }

  @Test
  public void testExtractDescription(){
    String description = sut.extractDescription("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-apricot-ripe---ready-320g.html");
    Assert.assertEquals("Apricots", description);
  }

  /**
   * This is the scrape fascade which is exposed to the user
   */
  @Test
  public void testScrape(){
    String jsonResponse = sut.scrape("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html");

    //test some result output
    JsonParser jsonParser = new JsonParser();

    JsonObject json = jsonParser.parse(jsonResponse).getAsJsonObject();
    JsonArray results = json.getAsJsonArray("results");
    Assert.assertTrue(results.size() == 7);


    Assert.assertTrue(json.has("total"));
    Assert.assertTrue(json.get("total").getAsDouble() == 15.10);

  }
}
